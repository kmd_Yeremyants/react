import { combineReducers } from 'redux';
import orders from './orders';
import startHome from './start_home';


export default combineReducers({
    orders,
    startHome
});
