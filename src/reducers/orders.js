const orders = (state, action) => {
    if(state === undefined) {
        state = [];
    }
    if(action.type === 'ADD_ORDERS') {
        state.push(action.orders);
    }
    return state;
}

export default orders;
