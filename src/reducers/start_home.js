const startHome = function(state, action) {
    if(action.type === 'GET_START_HOME') {
        return Object.assign(action.response);
    }
    return {
        orders: []
    }
}

export default startHome;
