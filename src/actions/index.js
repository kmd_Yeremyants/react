import axios from 'axios';

let startData = {
    count_orders_active: 4,
    count_orders_period: 3,
    count_users: 2,
    count_users_active: 1,
    orders: [
        {
            title: 'Название',
            price: '15000',
            text_preview: 'Описание',
            like: 10,
            dislike: 1,
            status: 1,
        },
        {
            title: 'Название 2',
            price: '10000',
            text_preview: 'Описание 2',
            like: 15,
            dislike: 3,
            status: 2,
        }
    ]
}

export function getHomeStart(dispatch) {
    return function(dispatch) {
        axios.get('http://127.0.0.1:8000/api/v1/stat-home/')
            .then(function (response) {
                // dispatch({ type: 'GET_START_HOME', response: response.data })
                dispatch({ type: 'GET_START_HOME', response: startData });
                return response;
            })
            .catch(function (error) {
                console.log(error);
            });
    }
}
