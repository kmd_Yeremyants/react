import React, { Component } from 'react';
import { connect } from 'react-redux';
import { SectionsContainer, Section, Header, Footer } from 'react-fullpage';
import './App.css';

let options = {
  sectionClassName:     'section',
  anchors:              ['sectionOne', 'sectionTwo', 'sectionThree'],
  scrollBar:            false,
  navigation:           true,
  verticalAlign:        false,
  sectionPaddingTop:    '50px',
  sectionPaddingBottom: '50px',
  arrowNavigation:      true,
  delay:                350,
};

class App extends Component {
  render() {
    let startData = this.props.startData;
    console.log(this.props);
    return (
      <div className="App">
        <Header>
          <a href="#sectionOne" className="opa">Section One</a>
          <a href="#sectionTwo">Section Two</a>
          <a href="#sectionThree">Section Three</a>
        </Header>
        <Footer>
          <a href="/" className="opa">Dcoumentation</a>
          <a href="/">Example Source</a>
          <a href="/">About</a>
        </Footer>
        <SectionsContainer {...options}>
          <Section color="#A7DBD8">
            Активных заказов - {startData.count_orders_active}<br/>
            Заданий за последние 30 дней - {startData.count_orders_period}<br/>
            Исполнителей в системе - {startData.count_users}<br/>
            Исполнителей в системе - {startData.count_users_active}<br/>
            <ul>
              <li>Заказы</li>
              {startData.orders.map((item, index) =>
                (
                  <li key={index}>
                    {item.title}, лайк {item.like}, дизлайн {item.dislike}, описание {item.text_preview}, статус {item.status}, цена {item.price}
                  </li>
                )
              )}
            </ul>
          </Section>
          <Section color="#dadada">Page 2</Section>
          <Section color="#E0E4CC">Page 3</Section>
        </SectionsContainer>
      </div>
    );
  }
}

const mapStateToProps = function(store) {
  return {
    startData: store.startHome
  }
}

export default connect(mapStateToProps)(App)
