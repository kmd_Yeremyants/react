import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import reducers from './reducers';
import { getHomeStart } from './actions';
import App from './App';
import './index.css';
import registerServiceWorker from './registerServiceWorker';

const store = createStore(
    reducers,
    composeWithDevTools(applyMiddleware(thunk))
);

store.dispatch(getHomeStart());

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);
registerServiceWorker();
